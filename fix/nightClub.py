import random
import string
import time


class Human:
    def __init__(self, man, rnb, electro, pop, name):
        self.man = man
        self.rnb = rnb
        self.electro = electro
        self.pop = pop
        self.name = name

class Dancing(object):
    def head(self):
        raise NotImplementedError()

    def torso(self):
        raise NotImplementedError()

    def hands(self):
        raise NotImplementedError()

    def body(self):
        raise NotImplementedError()

    def foot(self):
        raise NotImplementedError()

    def alco(self):
        raise NotImplementedError()

class Head(object):
    def __init__(self, whatDoing):
        self.whatDoing = whatDoing

    def __str__(self):
        return self.whatDoing

class Torso(object):
    def __init__(self, whatDoing):
        self.whatDoing = whatDoing

    def __str__(self):
        return self.whatDoing

class Hands(object):
    def __init__(self, whatDoing):
        self.whatDoing = whatDoing

    def __str__(self):
        return self.whatDoing

class Body(object):
    def __init__(self, whatDoing):
        self.whatDoing = whatDoing

    def __str__(self):
        return self.whatDoing

class Foot(object):
    def __init__(self, whatDoing):
        self.whatDoing = whatDoing

    def __str__(self):
        return self.whatDoing

class Alco(object):
    def __init__(self, whatDoing):
        self.whatDoing = whatDoing

    def __str__(self):
        return self.whatDoing


class Rnb(Dancing):
    def body(self):
        return Body('телом вперед-назад')

    def foot(self):
        return Foot('ноги в полу-присяде')

    def hands(self):
        return Hands('руки согнуты в локтях')

    def head(self):
        return Head('головой вперед-назад')


class Electrodance(Dancing):
    def torso(self):
        return Torso('туловищем вперед-назад')

    def head(self):
        return Head('почти нет движений головой')

    def hands(self):
        return Hands('круговые движения - вращения руками')

    def foot(self):
        return Foot('ноги двигаются в ритме')


class Pop(Dancing):
    def torso(self):
        return Torso('плавные движения туловищем')

    def hands(self):
        return Hands('плавные движения руками')

    def foot(self):
        return Foot('плавные движения ногами')

    def head(self):
        return Head('плавные движения головой')

class Alcohol(Dancing):
    def alco(self):
        return Alco('сидит в баре и пьет водку')


tracksWithStyle = {
    'Usher - Burn' : 'Rnb',
    'The Fugees - Ready Or Not' : 'Rnb',
    'Samual James - Magnasanti' : 'Electrohouse',
    'Spektrem - Shine' : 'Electrohouse',
    'Alan Walker - Sing Me To Sleep' : 'Pop',
    'Imagine Dragons - Thunder' : 'Pop'
}

def randstring(n):
    a = string.ascii_letters + string.digits
    return ''.join([random.choice(a) for i in range(n)])


if __name__ == '__main__':
    playList = []
    for track in tracksWithStyle:
        playList.append(track)

    count_dancer = random.randint(10,25)
    humans = []
    for i in range(0, count_dancer):
        h = Human(random.choice([True, False]),
                  random.choice([True, False]),
                  random.choice([True, False]),
                  random.choice([True, False]),
                  randstring(8))
        humans.append(h)
    print('В ночной клуб пришли:')
    for h in humans:
        if h.man:
            print(h.name, 'мальчик')
        else:
            print(h.name, 'девочка')
    time.sleep(5)
    print('')

    humansRnb = []
    humansElectro = []
    humansPop = []
    for h in humans:
        if h.rnb:
            humansRnb.append(h.name)
        if h.electro:
            humansElectro.append(h.name)
        if h.pop:
            humansPop.append(h.name)

    humansNotRnb = []
    humansNotElectro = []
    humansNotPop = []
    for h in humans:
        if not h.rnb:
            humansNotRnb.append(h.name)
        if not h.electro:
            humansNotElectro.append(h.name)
        if not h.pop:
            humansNotPop.append(h.name)

    for i in range(0, len(playList)):
        track = random.choice(playList)
        print(track, tracksWithStyle[track], 'Music')
        playList.remove(track)
        if tracksWithStyle[track] == 'Rnb':
            dance = Rnb()
            print(humansRnb, ':')
            print(dance.head(), ', ', dance.hands(), ', ', dance.body(), ', ', dance.foot())
            bar = Alcohol()
            print(humansNotRnb, ':')
            print(bar.alco())
            print('')
        if tracksWithStyle[track] == 'Electrohouse':
            dance = Electrodance()
            print(humansElectro, ':')
            print(dance.head(), ', ', dance.torso(), ', ', dance.hands(), ', ', dance.foot())
            bar = Alcohol()
            print(humansNotElectro, ':')
            print(bar.alco())
            print('')
        if tracksWithStyle[track] == 'Pop':
            dance = Pop()
            print(humansPop, ':')
            print(dance.head(), ', ', dance.torso(), ', ', dance.hands(), ', ', dance.foot())
            bar = Alcohol()
            print(humansNotPop, ':')
            print(bar.alco())
            print('')
        time.sleep(10)
print('вечеринка завершилась...')




